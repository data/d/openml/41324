# OpenML dataset: page-blocks-1-3_vs_4

https://www.openml.org/d/41324

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

A imbalanced version of the Page Blocks data set. where the possitive examples belong to class graphic and the negative examples belong to classes horiz.line and picture. (IR: 15.85)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41324) of an [OpenML dataset](https://www.openml.org/d/41324). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41324/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41324/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41324/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

